# **REST Architecture**
REpresentational State Transfer(REST) is a software architectural style that defines the constraints to create web services. Roy Fielding first presented it in 2000 in his famous dissertation.

> A Web API (or Web Service) conforming to the REST architectural style is a *REST API*.

## **Introduction**
The Representational State Transfer (REST) architectural style is not a technology you can purchase or a library you can add to your software development project. It is first and foremost a worldview that elevates information into a first class element of the architectures we build.

The ideas and terms we use to describe “RESTful” systems were introduced and collated in Dr. Roy Fielding’s thesis, “Architectural Styles and the Design of Network- based Software Architectures”. This document is academic and uses formal language, but remains accessible and provides the basis for the practice.

The summary of the approach is that by making specific architectural choices, we can elicit desirable properties from the systems we deploy. The constraints detailed in this architectural style are not intended to be used everywhere, but they are widely applicable.

Due to the Web’s prolific impact on consumer preferences, advocates of the REST style are encouraging organizations to apply the same principles within their boundaries as they do to external-facing customers with web pages. This will cover the basic constraints and properties found in modern REST web implementations. 

## **Guiding Principles of REST**
The six guiding principles or constraints of the RESTful architecture are:
### **1. Uniform Interface**
By applying the principle of generality to the components interface, we can simplify the overall system architecture and improve the visibility of interactions.

Multiple architectural constraints help in obtaining a uniform interface and guiding the behavior of components.

The following four constraints can achieve a uniform REST interface:
* Identification of resources – The interface must uniquely identify each resource involved in the interaction between the client and the server.
* Manipulation of resources through representations – The resources should have uniform representations in the server response. API consumers should use these representations to modify the resources state in the server.
* Self-descriptive messages – Each resource representation should carry enough information to describe how to process the message. It should also provide information of the additional actions that the client can perform on the resource.
* Hypermedia as the engine of application state – The client should have only the initial URI of the application. The client application should dynamically drive all other resources and interactions with the use of hyperlinks.

![uniform interface](
https://www.ics.uci.edu/~fielding/pubs/dissertation/uniform_ccss.gif
)

### **2. Client-server**
The client-server design pattern enforces the separation of concerns, which helps the client and the server components evolve independently.
By separating the user interface concerns (client) from the data storage concerns (server), we improve the portability of the user interface across multiple platforms and improve scalability by simplifying the server components.

While the client and the server evolve, we have to make sure that the interface/contract between the client and the server does not break.

![client-server](https://www.ics.uci.edu/~fielding/pubs/dissertation/client_server_style.gif)

### **3. Stateless**

Statelessness mandates that each request from the client to the server must contain all of the information necessary to understand and complete the request.

The server cannot take advantage of any previously stored context information on the server.

For this reason, the client application must entirely keep the session state.

![stateless](https://www.ics.uci.edu/~fielding/pubs/dissertation/stateless_cs.gif)

### **4. Cacheable**

The cacheable constraint requires that a response should implicitly or explicitly label itself as cacheable or non-cacheable.

If the response is cacheable, the client application gets the right to reuse the response data later for equivalent requests and a specified period.

![cacheable](https://www.researchgate.net/profile/Giulio-Trichilo/publication/319406504/figure/fig8/AS:533688267489286@1504252606966/FIGURA-40-VINCOLO-CLIENT-CACHE-STATELESS-SERVER-13.png)

### **5. Layered system**

The layered system style allows an architecture to be composed of hierarchical layers by constraining component behavior.

For example, in a layered system, each component cannot see beyond the immediate layer they are interacting with.

![layered-system](https://www.ics.uci.edu/~fielding/pubs/dissertation/layered_uccss.gif)

### **6. Code on demand (optional)**

REST also allows client functionality to extend by downloading and executing code in the form of applets or scripts.

The downloaded code simplifies clients by reducing the number of features required to be pre-implemented. Servers can provide part of features delivered to the client in the form of code, and the client only needs to execute the code.

![code on demand](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_style.gif)

### **What is a Resource?**
The key abstraction of information in REST is a resource. Any information that we can name can be a resource. For example, a REST resource can be a document or image, a temporal service, a collection of other resources, or a non-virtual object (e.g., a person).

The state of the resource, at any particular time, is known as the resource representation.

#### The resource representations are consist of:

* the data
* the metadata describing the data
* and the hypermedia links that can help the clients in transition to the next desired state.

> A REST API consists of an assembly of interlinked resources. This set of resources is known as the REST API’s resource model.
#### **Resource Identifiers**

REST uses resource identifiers to identify each resource involved in the interactions between the client and the server components.
### **Hypermedia or Hypertext**

The data format of a representation is known as a media type. The media type identifies a specification that defines how a representation is to be processed.

A RESTful API looks like hypertext. Every addressable unit of information carries an address, either explicitly (e.g., link and id attributes) or implicitly (e.g., derived from the media type definition and representation structure).

## **Define API operations in terms of HTTP methods**

The HTTP protocol defines a number of methods that assign semantic meaning to a request. The common HTTP methods used by most RESTful web APIs are:

* **GET** retrieves a representation of the resource at the specified URI. The body of the response message contains the details of the requested resource.
* **POST** creates a new resource at the specified URI. The body of the request message provides the details of the new resource. Note that POST can also be used to trigger operations that don't actually create resources.
* **PUT** either creates or replaces the resource at the specified URI. The body of the request message specifies the resource to be created or updated.
* **PATCH** performs a partial update of a resource. The request body specifies the set of changes to apply to the resource.
* **DELETE** removes the resource at the specified URI.

The effect of a specific request should depend on whether the resource is a collection or an individual item. The following table summarizes the common conventions adopted by most RESTful implementations using the e-commerce example. Not all of these requests might be implemented—it depends on the specific scenario.

| Resource | POST | GET | PUT | DELETE |
| :---: | :---: | :---: | :---: | :---:|
|/customers |	Create a new customer |	Retrieve all customers |	Bulk update of customers | 	Remove all customers |
|/customers/1 |	Error |	Retrieve the details for customer 1 |	Update the details of customer 1 if it exists |	Remove customer 1 |
|/customers/1/orders |	Create a new order for customer 1 |	Retrieve all orders for customer 1 |	Bulk update of orders for customer 1 | Remove all orders for customer 1 |
| 

## **Response Codes**

HTTP response codes give us a rich dialogue between clients and servers about the status of a request. Most people are only familiar with 200, 403, 404 and maybe 500 in a general sense, but there are many more useful codes to use. The tables presented here are not comprehensive, but they cover many of the most important codes you should consider using in a RESTful environment. Each set of numbers can be categorized as the following:

### 1XX: Informational
| code | DEscription |
| --- | --- |
| 100 | The client SHOULD continue with its request. This interim response is used to inform the client that the initial part of the request has been received and has not yet been rejected by the server. The client SHOULD continue by sending the remainder of the request or, if the request has already been completed, ignore this response. The server MUST send a final response after the request has been completed. |
| 101 | The server understands and is willing to comply with the client's request, via the Upgrade message header field (section 14.42), for a change in the application protocol being used on this connection. The server will switch protocols to those defined by the response's Upgrade header field immediately after the empty line which terminates the 101 response. |
### 2XX: Successful Client Requests
| code | DEscription |
| --- | --- |
| 200 |	OK. The request has successfully executed. Response depends upon the verb invoked.
| 201 |	Created. The request has successfully executed and a new resource has been created in the process. The response body is either empty or contains a representation containing URIs for the resource created. The Location header in the response should point to the URI as well.|
| 202 |	Accepted. The request was valid and has been accepted but has not yet been processed. The response should include a URI to poll for status updates on the request. This allows asynchronous REST requests |
| 204 |	No Content. The request was successfully processed but the server did not have any response. The client should not update its display. |
### 3XX: Redirected Client Requests
| code | DEscription |
| --- | --- |
|301 |	Moved Permanently. The requested resource is no longer located at the specified URL. The new Location should be returned in the response header. Only GET or HEAD requests should redirect to the new location. The client should update its bookmark if possible. |
|302 |	Found. The requested resource has temporarily been found somewhere else. The temporary Location should be returned in the response header. Only GET or HEAD requests should redirect to the new location. The client need not update its bookmark as the resource may return to this URL. |
|303 |	See Other. This response code has been reinterpreted by the W3C Technical Architecture Group (TAG) as a way of responding to a valid request for a non-network addressable resource. This is an important concept in the Semantic Web when we give URIs to people, concepts, organizations, etc. There is a distinction between resources that can be found on the Web and those that cannot. Clients can tell this difference if they get a 303 instead of 200. The redirected location will be reflected in the Location header of the response. This header will contain a reference to a document about the resource or perhaps some metadata about it. |
### 4XX: Invalid Client Requests
| code | DEscription |
| --- | --- |
| 405  |	Method Not Allowed. |
| 406  |	Not Acceptable. |
| 410  |	Gone. |
| 411  | 	Length Required. |
| 412  |	Precondition Failed. |
| 413  |	Entity Too Large. |
| 414  |	URI Too Long. |
| 415  |	Unsupported Media Type. |
| 417  |	Expectation Failed. |
### 5XX: Server Failed to Handle the Request
| code | DEscription |
| --- | --- |
| 500  |	Internal Server Error. |
| 501  |	Not Implemented. |
| 503  |	Service Unavailable. |
## **Summary**

In simple words, in the REST architectural style, data and functionality are considered resources and are accessed using Uniform Resource Identifiers (URIs).

The resources are acted upon by using a set of simple, well-defined operations. Also, the resources have to be decoupled from their representation so that clients can access the content in various formats, such as HTML, XML, plain text, PDF, JPEG, JSON, and others.

The clients and servers exchange representations of resources by using a standardized interface and protocol. Typically HTTP is the most used protocol, but REST does not mandate it.

Metadata about the resource is made available and used to control caching, detect transmission errors, negotiate the appropriate representation format, and perform authentication or access control.

And most importantly, every interaction with the server must be stateless.

All these principles help RESTful applications to be simple, lightweight, and fast.

## **References:**

* [REST APIs must be hypertext-driven](https://roy.gbiv.com/untangled/2008/rest-apis-must-be-hypertext-driven "REST APIs must be hypertext-driven")

* [REST Arch Style](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm "REST Arch Style")

* “RESTful Web APIs” by Leonard Richardson, Mike Amundsen and Sam Ruby, 2013. O’Reilly Media.

* RESTful Web Services Cookbook” by Subbu Allamaraju, 2010. O’Reilly Media.

* “REST in Practice” by Jim Webber, Savas Parastatidis and Ian Robinson, 2010. O’Reilly Media.